'''Works in some cases. Plan for rewrite:
Read in block by block, return type, meta, data.
Sort into (nested?) dictionaries.'''


import numpy as np
import pandas as pd
import io

def parsefile(filename,sortbycomments=False):
    if sortbycomments:
        data = {}

        with open(filename, 'r') as fn:
            for line in fn:
                if '[samip]' in line.lower():
                    parsesamip(fn)

                if '[spectrum]' in line.lower():
                    d,m = parsespectrum(fn)
                    if m['IDDataTypeSub1'].lower() == 'calibrated':
                        makestruct(data,'calibrated',m)
                        c1 =  m['CommentSub1'].replace(' ','')
                        c2 =  m['CommentSub2'].replace(' ','')
                        c3 =  m['CommentSub3'].replace(' ','')
                        if len(c3) > 0:
                            data['calibrated'][c1][c2][c3][datefrommeta(m)]\
                                                  = pd.Series(d[:,1], index=d[:,0])
                        else:
                            data['calibrated'][c1][c2][datefrommeta(m)]\
                                                  = pd.Series(d[:,1], index=d[:,0])
                    elif m['IDDataTypeSub1'].lower() == 'raw':
                        makestruct(data,'raw',m)
                        c1 =  m['CommentSub1'].replace(' ','')
                        c2 =  m['CommentSub2'].replace(' ','')
                        c3 =  m['CommentSub3'].replace(' ','')
                        if len(c3) > 0:
                            data['raw'][c1][c2][c3][datefrommeta(m)]\
                                                  = pd.Series(d[:,1], index=d[:,0])
                        else:
                            data['raw'][c1][c2][datefrommeta(m)]\
                                                  = pd.Series(d[:,1], index=d[:,0])
                                                  
                if '[pressure]' in line.lower():
                    d, m = parsepressure(fn)
                    c1 =  m['CommentSub1'].replace(' ','')
                    c2 =  m['CommentSub2'].replace(' ','')
                    c3 =  m['CommentSub3'].replace(' ','')
                    makestruct(data,'pressure',m)
                    if len(c3) > 0:
                        data['pressure'][c1][c2][c3][datefrommeta(m)]\
                                                  = pd.Series(d, index=['pressure'])
                    else:
                        data['pressure'][c1][c2][datefrommeta(m)]\
                                                  = pd.Series(d, index=['pressure'])
                
                if '[inclination]' in line.lower():
                    d, m = parseinclination(fn)
                    c1 =  m['CommentSub1'].replace(' ','')
                    c2 =  m['CommentSub2'].replace(' ','')
                    c3 =  m['CommentSub3'].replace(' ','')
                    makestruct(data,'inclination',m)
                    if len(c3) > 0:
                        data['inclination'][c1][c2][c3][datefrommeta(m)]\
                                                  = pd.Series(d, index=['ax1','ax2','incl'])
                    else:
                        data['inclination'][c1][c2][datefrommeta(m)]\
                                                  = pd.Series(d, index=['ax1','ax2','incl'])
        return data

        

    else:
        raw = pd.DataFrame()
        calibrated = pd.DataFrame()
        pressure = pd.DataFrame()
        inclination = pd.DataFrame()

        with open(filename, 'r') as fn:
            for line in fn:
                if '[samip]' in line.lower():
                    parsesamip(fn)

                if '[spectrum]' in line.lower():
                    data, meta = parsespectrum(fn)
                    if meta['IDDataTypeSub1'].lower() == 'calibrated':
                        calibrated[datefrommeta(meta)]  = pd.Series(data[:,1], index=data[:,0])
                    if meta['IDDataTypeSub1'].lower() == 'raw':
                        raw[datefrommeta(meta)] = pd.Series(data[:,1], index=data[:,0])

                if '[pressure]' in line.lower():
                    data, meta = parsepressure(fn)
                    pressure[datefrommeta(meta)] = pd.Series(data, index=['pressure'])
                
                if '[inclination]' in line.lower():
                    data, meta = parseinclination(fn)
                    inclination[datefrommeta(meta)] = pd.Series(data,index=['ax1','ax2','incl'])

        calibrated = calibrated.sort(axis=1)
        raw = raw.sort(axis=1)
    

        if len(pressure) > 0:
            pressure = pressure.sort(axis=1)
            inclination = inclination.sort(axis=1)

            return calibrated, raw, pressure, inclination

        return calibrated, raw, 'no pressure', 'no inclination'


def makestruct(data,key,meta):
    c1 =  meta['CommentSub1'].replace(' ','')
    c2 =  meta['CommentSub2'].replace(' ','')
    c3 =  meta['CommentSub3'].replace(' ','')
    if len(c3) > 0:
        if key not in data.keys():
            data[key] = {}
        if c1 not in data[key].keys():
            data[key][c1] = {}
        if c2 not in data[key][c1].keys():
            data[key][c1][c2] = {}
        if c3 not in data[key][c1][c2].keys():
            data[key][c1][c2][c3] = pd.DataFrame()
    else:
        if key not in data.keys():
            data[key] = {}
        if c1 not in data[key].keys():
            data[key][c1] = {}
        if c2 not in data[key][c1].keys():
            data[key][c1][c2] = pd.DataFrame()

def parsepressure(fileobj):
    meta, fo = getmetadata(fileobj)
    data = getdata(fo)

    fo.readline()
    
    return data, meta

def parseinclination(fileobj):
    meta, fo = getmetadata(fileobj)
    data = getdata(fo)
    fo.readline()

    return data, meta


    

def parsespectrum(fileobj):
    meta, fileob = getmetadata(fileobj)
    data = getdata(fileob,spectrum=True)

    fileobj.readline()
    
    return data, meta
    
def getmetadata(fileobj):
    meta = {}
    ni = True
    for line in fileobj:        
        if '[Attributes]' in line:
            meta['Attributes'] = {}
            for line in fileobj:
                
                if '[END]' in line:
                    break
                key,val = line.split('=')
                meta['Attributes'][key.strip()] = val.strip()

        elif '[END] of [Att' in line:
            pass
        elif '[data]' in line.lower():
            return meta, fileobj
        else:
            key, val = line.split('=')
            meta[key.strip()] = val.strip()

    

def getdata(fileobj,spectrum=False):
    s = ''
    if spectrum:
        fileobj.readline()
    for line in fileobj:        
        if '[END]' in line:
            return np.loadtxt(io.StringIO(s))
        s += line
            
        
def parsesamip(fileobj):
    for line in fileobj:
        if '[END] of SAMIP' in line or '[END] of [SAMIP]' in line:
            break

def datefrommeta(meta):
    return pd.datetime.strptime(meta['DateTime'],'%Y-%m-%d %H:%M:%S')
