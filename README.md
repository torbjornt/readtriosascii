Some simple functions for reading data from ASCII files in Trios'
native format. Only used with Ramses SAM/SAMIP sensors.

Could do with a complete rewrite, but works fine for the files
I've tested with. Data is placed in `pandas` `DataFrame` objects.

Dependencies:

 - `pandas`
 - `numpy`

Only tested with Python 3.4.

Licensed under the MIT license.