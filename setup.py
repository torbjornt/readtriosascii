from distutils.core import setup

setup(name='readTrios',
      version='0.1',
      description='Reading Trios data files',
      author='Torbjørn Taskjelle',
      author_email='Torbjorn.Taskjelle@ift.uib.no',
      py_modules=['readtrios']
      )
